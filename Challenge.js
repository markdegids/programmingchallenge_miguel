function print() { document.getElementById('output').innerHTML += (Array.prototype.slice.call(arguments, 0)).join(" ") + "<br>"; }

var data = "Lorem ipsum dolor sit amet, mauris ac dapibus accumsan, lacus eros aliquam est, quis lacinia lorem mi et nibh sed blandit est luctus et. Aenean laoreet vestibulum xy:10,20 placerat. Integer et sit vestibulum metus, a egestas mi et. Vestibulum porttitor tempor at interdum. Vestibulum scelerisque et erat eget lacus eleifend 30,10 lacinia. Nam sollicitudin sapxy:ien magna ac efficitur consectetur xy:15,25 sit xy:20,20 Phasellus lacus elit, Nulla et justo mi ac et urna 10,30 scelerisque ollis. Nam ex lacus lacus laoreet et eu ut est. Integer nulla eu purus orci. Nullam molestie justo quam, nec efficitur dui lacus non. Integer sed placerat purus. xy:20,10 Maecenas ac consectetur sapien.Pellentesque semper nisl et condimentum moll mi sapien, xy:10,10 egestas lacus sed quam in, molestie feugiat dui a, xy:10,20 malesuada venenatis erat. Integer ac risus diam. Integer commodo xy:eros et malesuada luctus pellentesque. Integer iaculis suscipit ligula vitae suscipit.";

/* Context on the goals and constraints
Since this is a programming challenge, my main objective will be communication.
I will focus on readability and succinctness without omitting obvious checks
needed in actual production code.
I comment where I made assumptions or where things could have been implemented differently.
For succinctness, I will omit the usual documentation which would be required if this
was code to share with the team.
I will also avoid changing the structure of the file (which is the reason I did not add any
tests. During development, I executed them separately).
*/

// Assertion on data not being null or undefined. If so, we do not proceed.
// How we handle this in production code could vary. For this exercise, I simply throw.
if (data == null) {
    throw new Error("data is null or undefined");
}


// (A) print the length of the string
print("Length of the string is :<br>" + (data.length) + "<br>");

// (B) print the number of dots in the string
function countPattern(text, pattern) {
    return text.match(new RegExp(pattern, "g"))?.length ?? 0;
}
print("Number of dots in the string is :<br>" + countPattern(data, /\./) + "<br>");

// (C) print the number of commas in the string
print("Number of commas in the string is :<br>" + countPattern(data, /,/) + "<br>");

// (D) print the top 5 reoccurring words an their occurrence rate
print("The Top5 of reoccurring words in the string is:");
function countWords(text) {
    /* Note: By words, we consider case insensitive sequences of letters, excluding the
       polygon markers "xy:". This was assumed from the example shown.*/
    var splitWords = text.match(new RegExp(/(?!(xy:))(\b[a-zA-Z]+\b)/, "g")) ?? [];
    var wordCount = new Map();
    for (var word of splitWords) {
        var lcWord = word.toLowerCase(); // Case is irrelevant. Stored as lower case to mimic example.
        if (wordCount.has(lcWord)) {
            wordCount.set(lcWord, wordCount.get(lcWord) + 1);
        } else {
            wordCount.set(lcWord, 1);
        }
    }
    // Return an ordered array. In this exercise, we never need to search for specific words.
    return [...wordCount].sort((a, b) => b[1] - a[1]);
}
var wourdCount = countWords(data);
var maxNumberOfWords = 5;
for (var i = 0; i < maxNumberOfWords && i < wourdCount.length; i++) {
    print(wourdCount[i][0] + " (" + wourdCount[i][1] + ")");
}
print("");


// (E) print the words that occur three times:
print("The words that occur three times in the string are:");
for (var word of wourdCount) {
    if (word[1] === 3) {
        print(word[0]);
    }
}
print("");

// (F) find all valid 'xy:' specified coordinates of the hidden polygon and print them
print("The xy coordinates of the hidden polygon are :");
/* Although not strictly required (a simple size 2 array would do), I made the Point class
 because it is convenient when handling geometric objects. Moreover, the notation helps
 avoid errors later when implementing the polygon functions.*/
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    equals(other) {
        return (this.x === other.x) && (this.y === other.y);
    }
    toString() {
        return "[" + this.x.toFixed(2) + "," + this.y.toFixed(2) + "]";
    }
}
function findCoordinates(text) {
    /* Current parsing assumes the coordinates are integers with no spaces. This was assumed because it is what
     the example seems to indicate. In actual development, this would need to be clarified, of course.
     We can replace ([-+]?\d+) by ([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?) to find floating point numbers. */
    var coordPairs = text.match(new RegExp(/(?<=xy:)([-+]?\d+),([-+]?\d+)/, "g")) ?? [];
    var points = new Array();
    var i = 0;
    for (var coordPair of coordPairs) {
        var splitCoord = coordPair.split(',');
        points[i++] = new Point(parseFloat(splitCoord[0]), parseFloat(splitCoord[1]));
    }
    return points;
}

var polygon = findCoordinates(data);
for (var vertex of polygon) {
    print(vertex.toString());
}
print("");

// (G) print the area of the polygon defined in (F)
print("The area of the polygon is:");
function validatePolygon(polyVertices) {
    /* Note 1: Polygon validation.
      Understandably, we do not have a clear definition of what constitutes valid polygons in the exercise.
      Different definitions would imply different algorithmic implementations.
      This would be clarified within the team before proceeding, in actual development.
      With this validation, I avoid calculating areas and centroids for invalid inputs. */
    /* Note 2: Polygon definition.
      For the purposes of the exercise, I will consider that the polygon must be a non-intersecting polygon.
      The sides are defined by consecutive points found in the text. There must be at least 2 line segments
      and the segments must close (the last vertex found must equal the first).
      This is what the example suggests and it also aligns with the requirements of the algorithms
      implemented below.*/
    function intersect(p1, p2, p3, p4) {
        function crossProductSign(p1, p2, a) {
            var v1 = new Point(p2.x - p1.x, p2.y - p1.y);
            var v2 = new Point(a.x - p1.x, a.y - p1.y);
            var crossProduct = v1.x * v2.y - v1.y * v2.x;
            return Math.sign(crossProduct);
        }
        function onSegment(p1, p2, a) {
            // Assumes prior test on colinearity
            if (a.equals(p1) || a.equals(p2)) {
                // Ignore the intersection of segment ends. That is intended.
                return false;
            }
            return (Math.min(p1.x, p2.x) <= a.x && a.x <= Math.max(p1.x, p2.x))
                && (Math.min(p1.y, p2.y) <= a.y && a.y <= Math.max(p1.y, p2.y));
        }

        d1 = crossProductSign(p3, p4, p1);
        d2 = crossProductSign(p3, p4, p2);
        d3 = crossProductSign(p1, p2, p3);
        d4 = crossProductSign(p1, p2, p4);

        if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0))
            || ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
            return true;
        }
        return ((d1 == 0 && onSegment(p3, p4, p1))
            || (d2 == 0 && onSegment(p3, p4, p2))
            || (d3 == 0 && onSegment(p1, p2, p3))
            || (d4 == 0 && onSegment(p1, p2, p4)));
    }

    if (polyVertices.length < 3) {
        // Must at least have 2 line segments.
        return false;
    }
    if (!polyVertices[0].equals(polyVertices[polyVertices.length - 1])) {
        // The line defining the polygon must close.
        return false;
    }

    /* Test for intersections. This algorithm is O(n^2), but that seems acceptable
     in the context. The simple double loop is much more readable and compact than
     more advanced algorithms such as line sweep.*/
    var indexForLastSegment = polyVertices.length - 2;
    for (var i = 0; i <= (indexForLastSegment - 1); i++) {
        for (var j = i + 1; j <= indexForLastSegment; j++) {
            if (intersect(polyVertices[i], polyVertices[i + 1], polyVertices[j], polyVertices[j + 1])) {
                return false;
            }
        }
    }

    return true;
}
function calculateSignedAreaShoelaceMethod(polyVertices) {
    /* I keep the sign in this function since signed areas can be useful for orientation.
     Orientation of areas can be useful in many circumstances. (Back-face culling comes to mind even tho
     its common implementation is with surface normals). In our case, it is useful since using the signed
     area simplifies the centroid function below. */
    var area = 0;
    for (var i = 0; i < polyVertices.length - 1; i++) {
        area += (polyVertices[i].x * polyVertices[i + 1].y) - (polyVertices[i + 1].x * polyVertices[i].y);
    }
    return (area / 2.0);
}
function calculateArea(polyVertices) {
    return Math.abs(calculateSignedAreaShoelaceMethod(polyVertices));
}

if (validatePolygon(polygon)) {
    print(calculateArea(polygon) + "<br>");
} else {
    print("Vertices found do not create a valid polygon." + "<br>");
}

// (H) print the centroid of the polygon defined in (F)
print("The centroid of the polygon is:");
function polygonCentroid(polyVert) {
    /* Currently, 0 area polygons are being allowed in the validate function.
     For these, this function returns NaN. In actual production, this situation would be
     clarified alongside the definition of a polygon. */
    var centroid = new Point(0.0, 0.0);
    var area = calculateSignedAreaShoelaceMethod(polyVert);
    for (var i = 0; i < polyVert.length - 1; i++) {
        centroid.x += (polyVert[i].x + polyVert[i + 1].x) * ((polyVert[i].x * polyVert[i + 1].y) - (polyVert[i + 1].x * polyVert[i].y));
        centroid.y += (polyVert[i].y + polyVert[i + 1].y) * ((polyVert[i].x * polyVert[i + 1].y) - (polyVert[i + 1].x * polyVert[i].y));
    }

    centroid.x = centroid.x / (6 * area);
    centroid.y = centroid.y / (6 * area);
    return centroid;
}

if (validatePolygon(polygon)) {
    print(polygonCentroid(polygon).toString());
} else {
    print("Vertices found do not create a valid polygon." + "<br>");
}
